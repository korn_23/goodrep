import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class testSquarethree {
    @Test(expected = IllegalArgumentException.class)
    public void testDLowerThanZeroOrAIsZero(){
        Squarethree st = new Squarethree(0,1,1);
        assertNull(st.getRoots());
    }

    @Test
    public void testDEqualsZero(){
        Squarethree st = new Squarethree(1,2,1);
        assertEquals(st.getRoots()[0],-1, 10E-9);
        assertEquals(st.getRoots()[1],-1,10E-9);
    }

    @Test
    public void testDBiggerThanZero(){
        Squarethree st = new Squarethree(1,2,-3);
        assertEquals(st.getRoots()[0],1, 10E-9);
        assertEquals(st.getRoots()[1],-3,10E-9);
    }
}
